<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tentukan Nilai</title>
</head>
<body>
    <?php
        echo "<h2>1. Tentukan Nilai</h2>";

        function tentukan_nilai($number) {
            if ($number >= 85 && $number <= 100) {
                echo "Sangat baik <br>";
            } else if ($number >= 70 && $number < 85) {
                echo "Baik <br>";
            } else if ($number >= 60 && $number < 70) {
                echo "Cukup <br>";
            } else {
                echo "Kurang <br>";
            }
        }

        //TEST CASES
        echo tentukan_nilai(98); //Sangat Baik
        echo tentukan_nilai(76); //Baik
        echo tentukan_nilai(67); //Cukup
        echo tentukan_nilai(43); //Kurang

        echo "<h2>2. Ubah Huruf</h2>";

        function ubah_huruf($string){
            // kode disini
            for ($i = 0; $i < strlen($string); $i++) {
                // ord() konversi string ke angka
                // chr() konversi angka ke string
                $new_string[$i] = chr(ord($string[$i]) + 1);
            }
            return implode($new_string) . '<br>';
        }
            
            // TEST CASES
            echo ubah_huruf('wow'); // xpx
            echo ubah_huruf('developer'); // efwfmpqfs
            echo ubah_huruf('laravel'); // mbsbwfm
            echo ubah_huruf('keren'); // lfsfo
            echo ubah_huruf('semangat'); // tfnbohbu

        echo "<h2>3. Tukar Besar Kecil</h2>";

        function tukar_besar_kecil($string){
            $output = "";
            for ($i = 0; $i < strlen($string); $i++){
                //ctype_upper() untuk mengecek kondisi true apabila hurufnya besar
                if (ctype_upper($string[$i])){
                    //strtolower() untuk menjadikan string huruf kecil
                    $output .= strtolower($string[$i]);
                } else {
                    //strtolower() untuk menjadikan string huruf besar
                    $output .= strtoupper($string[$i]);
                }
            }
            //implode() untuk konversi array ke string
            return $output . "<br>";
        }

        // TEST CASES
        echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
        echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
        echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
        echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
        echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

    ?>
</body>
</html>